<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ive-group' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'WP_HOME', 'http://localhost/dev/ive-group' );
define( 'WP_SITEURL', 'http://localhost/dev/ive-group' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'LN/(h0[,qL$1gzfdE)=mDHbK#jfDFn+qP1IRW/SVY}!y]Rqd*@jd!6eP{%3chHV2' );
define( 'SECURE_AUTH_KEY',  'K`[1W?5)=2y um9C1|opYEd(K{|V(X:ur(IzdC.nLrL-u=Hi?a2aOY]azNk4#FD^' );
define( 'LOGGED_IN_KEY',    'Dm8lmB[:8Ht8!aqmh60[5x%I0D?#E-o$vn/@2r5ZuBxr,~J6D2u}qb/T=&e6C$g0' );
define( 'NONCE_KEY',        '%hlG5EgA|oQk$PaZyKa{tAi*X[tSV$E+ukwzZ5#-Y<n==C_+;M$Nd47.)I5/0 4s' );
define( 'AUTH_SALT',        '0|Z*Qr/MnBU+zPGd8^o$Zf?fcRE?.:}L`p]|vWu4DzDL/Td;wM<x.60Msa:f*&tz' );
define( 'SECURE_AUTH_SALT', 'KE~y*(0Mfah0;?p>usknSmonvBXGEHy,l_P@Y7l+/<~fg$-8d|e1>S2(c1I<Ulk(' );
define( 'LOGGED_IN_SALT',   '}ei!WsVJcI1b<0Q-Z)zmH9^b2$4<}{Uhild&HPE6->>b$t8G7BXW_m,Dt.^5<PJ~' );
define( 'NONCE_SALT',       '*hI>q%{@TKFNtH}8`R832v$)+Uk~`4GC2>:|S&tAIbDY_u53gPqoEOU<xqWe-s1p' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ive_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
Plugin Name: Stylesheet Override
Plugin URI:  http://wordpress.com
Description: Override styles by adding a stylesheet
Version:     1.0
Author:      Chris Asher
Author URI:  http://oinkdigital.com.au
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/
add_action( 'wp_enqueue_scripts', 'loadOverrideScripts' );
function loadOverrideScripts(){
	wp_enqueue_style('stylesheet',plugins_url('css/style.css',__FILE__));
}
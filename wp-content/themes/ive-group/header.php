<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>


<body <?php body_class(); ?>>
<div class="mobile-header block lg:hidden">
    <div class="logo-holder">
		<a href=" <?php echo get_home_url(); ?>">
			<svg class="mobile-site-logo" width="70" height="68" viewBox="0 10 120 68" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" clip-rule="evenodd" d="M97.6213 30.4756C103.216 30.4756 107.312 33.6504 107.911 39.4246H86.832C87.6316 34.2277 91.5269 30.4756 97.6213 30.4756ZM110.25 52.7716C107.208 55.6121 103.275 57.0323 98.9202 57.0323C93.8251 57.0323 87.9313 55.0122 86.8322 48.7576H119.6C119.9 46.3513 120 44.5233 120 42.9844C120 27.6848 109.61 20.0835 98.0218 20.0835C85.9328 20.0835 73.8447 28.3582 73.8447 44.139C73.8447 60.112 86.2323 67.8094 98.8204 67.8094C107.112 67.8094 114.505 64.5385 119.6 58.3801L119.597 58.3763L110.25 52.7716Z" fill="#031D51"/>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.51611 66.847H14.3039V21.2373H1.51611V66.847Z" fill="#031D51"/>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M69.8595 30.2033V21.2373H59.4054L46.2532 50.7779L32.9822 21.2373H22.5244V30.1957L40.2586 66.847H52.147L69.8595 30.2033Z" fill="#031D51"/>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M7.81791 0C12.1408 0 15.8204 3.45548 15.8204 7.61919C15.8204 11.6944 12.1408 15.1489 7.81791 15.1489C3.58733 15.1489 0 11.6944 0 7.61919C0 3.45548 3.58733 0 7.81791 0Z" fill="#031D51"/>
			</svg>
		</a>
	</div>
    <div id="menu_open" class="menu__icon mobile cursor-pointer"></div>
</div>
<?php get_template_part( 'template-parts/post/mobile-menu', get_post_format() ); ?>

<nav id="top">
	<?php if(is_user_logged_in()): $addClass = 'hidden'; endif; ?>
	<div class="navigation-holder flex justify-between items-center px-3 hidden lg:flex" id="nav-bar">
		<div class="first-parent flex">
			<div class="logo">
				<a id="logo" href="<?php echo get_home_url(); ?>">
					<svg class="hidden lg:flex site-logo" width="78" height="68" viewBox="0 10 120 68" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd" d="M97.6213 30.4756C103.216 30.4756 107.312 33.6504 107.911 39.4246H86.832C87.6316 34.2277 91.5269 30.4756 97.6213 30.4756ZM110.25 52.7716C107.208 55.6121 103.275 57.0323 98.9202 57.0323C93.8251 57.0323 87.9313 55.0122 86.8322 48.7576H119.6C119.9 46.3513 120 44.5233 120 42.9844C120 27.6848 109.61 20.0835 98.0218 20.0835C85.9328 20.0835 73.8447 28.3582 73.8447 44.139C73.8447 60.112 86.2323 67.8094 98.8204 67.8094C107.112 67.8094 114.505 64.5385 119.6 58.3801L119.597 58.3763L110.25 52.7716Z" fill="#031D51"/>
						<path fill-rule="evenodd" clip-rule="evenodd" d="M1.51611 66.847H14.3039V21.2373H1.51611V66.847Z" fill="#031D51"/>
						<path fill-rule="evenodd" clip-rule="evenodd" d="M69.8595 30.2033V21.2373H59.4054L46.2532 50.7779L32.9822 21.2373H22.5244V30.1957L40.2586 66.847H52.147L69.8595 30.2033Z" fill="#031D51"/>
						<path fill-rule="evenodd" clip-rule="evenodd" d="M7.81791 0C12.1408 0 15.8204 3.45548 15.8204 7.61919C15.8204 11.6944 12.1408 15.1489 7.81791 15.1489C3.58733 15.1489 0 11.6944 0 7.61919C0 3.45548 3.58733 0 7.81791 0Z" fill="#031D51"/>
					</svg>
				</a>
			</div>
		</div>
<!--		<div id="mobile-nav" class="mobile-menu flex-col lg:hidden overflow-hidden relative">-->
<!--		</div>-->
		<div class="second-parent flex items-center hidden lg:flex lg:-ml-2 block">
			<?php
				wp_nav_menu(array('theme_location' => 'primary'));
			?>
		</div>
		<div class="hidden lg:flex third-parent relative">
			<?php
				wp_nav_menu(array('theme_location' => 'secondary'));
			?>
		</div>
	</div>
	<div id="spacer" class="<?php echo $addClass ?>"></div>
</nav>
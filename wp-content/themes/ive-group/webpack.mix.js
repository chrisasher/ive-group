const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.options({ imgLoaderOptions: { enabled: false } })
    .sourceMaps()
    .webpackConfig({ devtool: 'source-map' })
    .js('main.js', 'js')
    .sass('sass/style.scss', 'css')
    .sass('sass/dashboard.scss', '../')
    .setPublicPath('./dist')
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.config.js') ],
    })
    .browserSync({
        proxy: 'localhost:8080/dev/ive-group',
        files: [
         'sass/**/*',
         '*.php',
         '*.js',
        ]
    })
/**
 * Created by chris on 12/04/2016.
 */
jQuery(document).ready(function($){
	makesquare('.square');

	// sticky
	if($(window).width() > 768) {

	$('.gal-sticky').stickySidebar({
		topSpacing: 100,
		bottomSpacing: 100,
		containerSelector: '#gallery-container'
	});

	$('.below-fold').addClass('to-fade-in');
 }
	
	$('.next-level').click(function (e) {
		e.preventDefault();
		$(this).next('.m-sub-menu').addClass('active');
	});
	$('.m-back-button').click(function (e) {
		e.preventDefault();
		$(this).parent().removeClass('active');
	});


	$('.logo').addClass('show-logo');

	if($('body').hasClass('home')) {
		$('#video-modal')[0].pause();
		console.log('whats this');
	}

	$('.menu-main-menu-container').find('#menu-main-menu').addClass('top-level');

	// if($('#header-content-block').hasClass('purple-block')) {
	// 	$('body').css('margin-top', '0');
	// 	$('.mobile-menu-header').addClass('transparent');
	// }

	let navbar = $('.navigation-holder');
	let mobNav = $('.mobile-header');

	if($('body').hasClass('page-template-page_our-services')) {
		$('#header-content-block').addClass('services-overview-page');
	}

	if($('#header-content-block').hasClass('darkblue-block')) {
		$('#services-below-fold').addClass('darkblue-border');
	}

	if($('#header-content-block').hasClass('purple-block')) {
		$('#services-below-fold').addClass('purple-border');
	}

	if($('#header-content-block').hasClass('lightblue-block')) {
		$('#services-below-fold').addClass('lightblue-border');
	}

	if($('#header-content-block').hasClass('turquoise-block')) {
		$('#services-below-fold').addClass('turquoise-border');
	}

	if($('#header-content-block').hasClass('blue-block')) {
		$('#services-below-fold').addClass('blue-border');
	}

	if($('.header-content-block').hasClass('text-white')) {
		navbar.addClass('white-header');
		mobNav.addClass('white-header');
	} else if($('.header-content-block').hasClass('text-blue')) {
		navbar.addClass('blue-header');
		mobNav.addClass('blue-header');
	}

	if($('body').hasClass('page-template-page_our-services')) {
		navbar.removeClass('white-header');
		mobNav.removeClass('white-header');
		navbar.addClass('services-page');
	}

	if($('body').hasClass('page-template-page_about')) {
		$('body').addClass('about-overview');
		navbar.removeClass('blue-header');
		navbar.removeClass('white-header');
		mobNav.removeClass('blue-header');
		mobNav.removeClass('white-header');
	}
	
	if($('body').hasClass('page-template-default')) {
		$('body').addClass('regular-page');
		navbar.removeClass('blue-header');
		navbar.removeClass('white-header');
		mobNav.removeClass('blue-header');
		mobNav.removeClass('white-header');
	}

	$("body").swipe({
        swipeLeft: function(event, direction, distance, duration, fingerCount) {
            $(".mfp-arrow-right").magnificPopup("prev");
        },
        swipeRight: function() {
            $(".mfp-arrow-left").magnificPopup("next");
        },
		threshold: 50,
		excludedElements: "label, button, input, select, textarea, .noSwipe"
    });

	$('body').css('margin-bottom', $('.navigation-header').height());

	function checkScreen(elem) {
		// if the element doesn't exist, abort
		if( elem.length == 0 ) {
			return;
		}
		var $window = jQuery(window)
		var viewport_top = $window.scrollTop()
		var viewport_height = $window.height()
		var viewport_bottom = viewport_top + viewport_height
		var $elem = jQuery(elem)
		var top = $elem.offset().top + 100
		var height = $elem.height()
		var bottom = top + height
	
		return top <= viewport_bottom
	}

	function isOnScreen(elem) {
		// if the element doesn't exist, abort
		if( elem.length == 0 ) {
			return;
		}
		var $window = jQuery(window)
		var viewport_top = $window.scrollTop()
		var viewport_height = $window.height()
		var viewport_bottom = viewport_top + viewport_height
		var $elem = jQuery(elem)
		var top = $elem.offset().top + $elem.height()
		var height = $elem.height()
		var bottom = top + height
	
		return (top >= viewport_top && top < viewport_bottom) ||
		(bottom > viewport_top && bottom <= viewport_bottom) ||
		(height > viewport_height && top <= viewport_top && bottom >= viewport_bottom)
	}

	$('.gif-animate').hide();

	$(document).on('scroll', function(){
		let headerPosition = $(document).scrollTop();
		if(headerPosition > 30) {
            $('.navigation-holder').addClass('shrink');
            $('.mobile-header').addClass('shrink');
        } else {
			$('.navigation-holder').removeClass('shrink');
			$('.mobile-header').removeClass('shrink');
		}
		if($('body').hasClass('home')) {
			if(isOnScreen($('#marketing-video'))) {
				$('#marketing-video')[0].play();
			} else {
				$('#marketing-video')[0].pause();
			}
		}

		if(checkScreen($('.sub-services.to-fade-in'))) {
			$('.sub-services').addClass('faded-in');
		} else {
			$('.sub-services').removeClass('faded-in');
		}

		if(checkScreen($('.below-fold.to-fade-in'))) {
			$('.below-fold').addClass('faded-in');
		} else {
			$('.below-fold').removeClass('faded-in');
		}

		if(checkScreen($('.services-menu.to-fade-in'))) {
			$('.services-menu').addClass('faded-in');
		} else {
			$('.services-menu').removeClass('faded-in');
		}

		if(checkScreen($('.latest-blog.to-fade-in'))) {
			$('.latest-blog').addClass('faded-in');
		} else {
			$('.latest-blog').removeClass('faded-in');
		}

		if(checkScreen($('.fade.to-fade-in'))) {
			$('.fade').addClass('faded-in');
		} else {
			$('.fade').removeClass('faded-in');
		}
		
		if(checkScreen($('.content-holder.to-fade-in'))) {
			$('.content-holder').addClass('faded-in');
		} else {
			$('.content-holder').removeClass('faded-in');
		}

		// About pages transitions
		if(checkScreen($('.single-about-content.to-fade-in'))) {
			$('.single-about-content').addClass('faded-in');
		} else {
			$('.single-about-content').removeClass('faded-in');
		}

		if(checkScreen($('.more-services.to-fade-in'))) {
			$('.more-services').addClass('faded-in');
		} else {
			$('.more-services').removeClass('faded-in');
		}

		if(checkScreen($('.more-sub-services.to-fade-in'))) {
			$('.more-sub-services').addClass('faded-in');
		} else {
			$('.more-sub-services').removeClass('faded-in');
		}

		if(checkScreen($('.news-grid.to-fade-in'))) {
			$('.news-grid').addClass('faded-in');
		} else {
			$('.news-grid').removeClass('faded-in');
		}

		if(checkScreen($('.ive-switchable-columns.to-fade-in'))) {
			$('.ive-switchable-columns').addClass('faded-in');
		} else {
			$('.ive-switchable-columns').removeClass('faded-in');
		}

		if(isOnScreen($('.two-column'))) {
			$('.gif-animate').show();
		}
		
		if(checkScreen($('.contact-block.to-fade-in'))) {
            $('.contact-block').addClass('faded-in');
        } else {
            $('.contact-block').removeClass('faded-in');
		}
		
		if(checkScreen($('.box'))) {
			$('.box').addClass('reveal');
		} else {
			$('.box').removeClass('reveal');
		}

		if($(window).width() < 768) {
			if(isOnScreen($('.gif-animate'))) {
				$('.gif-animate').show();
			}
		}
	})

	// Get header height
	if($('body').hasClass('home') && $('body').hasClass('blog')) {
		var height = $('.navigation-holder').height();
		$('#spacer').css('height', height);
	}

	$('.back-to-top').click(function(e){
		e.preventDefault();
		$('html, body').animate({scrollTop:0}, '600');
	})

	// Mobile menu behavior
	var left, width, newLeft;

	$('.menu__icon').click(function(){
		$('.m-sub-menu').removeClass('active');

		if(!$('body').hasClass('menu_shown')) {
			$('body').addClass('menu_shown')
		} else {
			$('body').removeClass('menu_shown');
			$('.menu-list').removeClass('slide-left');
			$('.has-child-menu').addClass('hide-it');
			$('.has-child-menu').removeClass('slide-right');
			$('.back-button').hide();
		}

		var navLinks = $('ul#menu-main-menu li');
	});

	// New MENU

	$('.has-submenu .above-with-items').click(function(e) {
		
		// transform the parent
		$('.above-with-items').addClass('parent-transform');
		$('.above').addClass('parent-transform');
		
		// then get the children 
		$(this).next().addClass('slide-left');
		$(this).next().addClass('current-menu');

		// get the third level
		$('.current-menu').find('.second-has-submenu').click(function(){
			
			$(this).find('.third-sub-menu').addClass('slide-third');
			
			$('.second-back-button').hide();

			$('.second-below').addClass('slide-third');

			// Colors for mobile background
			if($('.third-sub-menu.lightblue-secondary').hasClass('slide-third')) {
				console.log('it does');
				$('.navigation-holder').addClass('lightblue');
			} else {
				$('.navigation-holder').removeClass('lightblue');
			}		

			if($('.third-sub-menu.purple-secondary').hasClass('slide-third')) {
				console.log('it does');
				$('.navigation-holder').addClass('purple');
			} else {
				$('.navigation-holder').removeClass('purple');
			}		

			if($('.third-sub-menu.turquoise-secondary').hasClass('slide-third')) {
				console.log('it does');
				$('.navigation-holder').addClass('turquoise');
			} else {
				$('.navigation-holder').removeClass('turquoise');
			}		

			if($('.third-sub-menu.blue-secondary').hasClass('slide-third')) {
				console.log('it does');
				$('.navigation-holder').addClass('blue');
			} else {
				$('.navigation-holder').removeClass('blue');
			}		
			
		});

	});

	// Handle back button clicks
	$('.has-submenu .second-back-button').click(function(e){
		$(this).parent().removeClass('slide-left');
		$('.above-with-items').removeClass('parent-transform');
		$('.above').removeClass('parent-transform');
	})

	// Handle third level button click
	$('.third-back-button').on('click', function(){
		$('.third-menu-item').addClass('slide-left');
	})

	if($('.third-sub-menu').hasClass('slide-third')) {
		console.log('it does');
		$('.navigation-holder').addClass('lightblue');
	} else {
		$('.navigation-holder').removeClass('lightblue');
	}


	// New MENU END

	var currTitle = $('.sitewide-header').html();

	// Magnific Popup
	$('.gallery').each(function(){
		$(this).magnificPopup({
			delegate: 'a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile mfp-with-zoom',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>', // markup of an arrow button
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			removalDelay: 900,
			callbacks: {
				beforeOpen: function() {
					this.st.mainClass = this.st.el.attr('data-effect');
				}
			  },
			zoom: {
				enabled: true, // By default it's false, so don't forget to enable it
			
				duration: 300, // duration of the effect, in milliseconds
				easing: 'ease-in-out', // CSS transition easing function

				opener: function(openerElement) {
				  return openerElement.is('a') ? openerElement : openerElement.find('a');
				}
			  },
			image: {
				markup: 
				'<div class="mfp-figure">'+
				'<div class="mfp-close"></div>'+
				'<div class="gallery-title container w-5/6 text-center mb-3 text-white">'+ 'Gallery for '+ currTitle +'</div>' +
						'<div class="mfp-counter">Image</div>'+
								'<div class="mfp-img"></div>'+
					'<div class="mfp-bottom-bar">'+
						  '<div class="mfp-title w-full lg:w-1/2 pt-2 pr-0 mx-auto text-center"></div>'+
					'</div>'+
			  '</div>',
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title') + '';
				}
			}
		});
	})

	// Contact page tabs

	if($('body').hasClass('page-template-page_contact')) {
		let tabLinks = $('.tab-selection a');
		var content = $('.tab-content-section .tab-content');
		tabLinks[0].classList.add('active');
		content[0].classList.add('active');

		// Handle link clicks.
		tabLinks.click(function(event) {
		var $this = $(this);
		
		// Prevent default click behaviour.
		event.preventDefault();
		
		// Remove the active class from the active link and section.
		$('.tab-content.active, .tab-selection a.active').removeClass('active');
		
		// Add the active class to the current link and corresponding section.
		$this.addClass('active');
		$($this.attr('href')).addClass('active');
	});
	}

	$("video").prop('muted', true); 
});

// Modal open and close for video
const modal = document.querySelector('.modal');
const playButton = document.querySelector('.play-button');
const video = document.querySelector('.video-holder');
const closeButton = document.querySelector('.close-button');

if(jQuery('body').hasClass('home')) {
	playButton.addEventListener('click', () => {
		document.querySelector('#video-modal').muted = false;
		modal.classList.add('open');
		video.classList.add('open');
		document.querySelector('#video-modal').play();
		document.querySelector('.onpage-video').pause();
	})
	
	closeButton.addEventListener('click', (e) => {
		modal.classList.remove('open');
		video.classList.remove('open');
		document.querySelector('.onpage-video').play();
		document.querySelector('#video-modal').pause();
	})
	
	modal.addEventListener('click', (e) => {
		if(e.target.classList.contains('modal')) {
			modal.classList.remove('open');
			video.classList.remove('open');
			document.querySelector('#video-modal').pause();
			document.querySelector('.onpage-video').play();
		}
	})
}

let posts = document.querySelectorAll('.news-block-post');

// var postsArray = Array.from(posts);


jQuery(window).load(function($){
	makesquare('.square');
})

jQuery(window).resize(function($){
	makesquare('.square');
	// var h = $('.light-blue').height();

	// $('.onpage-video').css('height', h);

})

function sizetorow(divheight, target, adjustment){
	var $ = jQuery;
	if($(window).width() > 800){
		var maxHeight = 0;

		$(target).css('min-height',maxHeight);
		var maxHeight = $(divheight).height() - adjustment;
		console.log(maxHeight);
		//$(target).each(function(){
		//
		//})
		setTimeout(function(){
			$(target).css('min-height',maxHeight);
		},600)
	}
}

function makesquare(target){
	var $ = jQuery;
	var width = $(target).width();
	$(target).height(width);
}
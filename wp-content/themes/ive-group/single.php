<?php get_header();

$contactHeading = get_theme_mod('contact_heading');
$contactImage = get_theme_mod('contact_image');
$contactShortcode = get_theme_mod('contact_shortcode');

$post_date = get_the_date( 'j F, Y' );
$image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg';
?>
<main>
	<section class="article">
		<div class="article-header py-5 lg:pt-7">
			<div class="container relative">
				<div class="w-4/5 mx-auto">
					<p class="date"><?php echo $post_date ?></p>
					<h1 class="text-white w-full lg:w-3/4"><?php the_title() ?></h1>
				</div>
			</div>
		</div>
		<div class="container lg:pt-8 lg:pb-7 article-holder relative">
			<div class="w-4/5 mx-auto">
				<img class="absolute hidden lg:block" src="<?php echo get_template_directory_uri() ?>/dist/images/article-frame.svg" alt="">
				<div class="article-image" style="background-image: url('<?php echo $image ?>'); background-repeat: no-repeat;
				background-position: center;
				background-size: cover;
				height: 540px;"></div>
			</div>
			<?php
			if(have_posts()){
				while ( have_posts() ){
					the_post();
					// $title = get_the_title();
					// $id = get_the_ID();
					// $bodyclass = get_body_class();
					?>

					<!-- date -->
					<div class="w-3/5 mx-auto pt-4 lg:py-7 article-content">
						<?php the_content(); ?>
					</div>
					<?php
				}
			}
			?>
			<div class="navigation lg:pt-5">
				<div id="cooler-nav" class="flex justify-between">
					<?php
                    $prevPost = get_previous_post(true);
					$prevLink = get_permalink(get_adjacent_post(false,'',true));
					if($prevPost) {?>

						<div class="nav-box previous flex desktop w-1/2">
							<a class="arrow-link hidden lg:block prev" href="<?php echo $prevLink ?>"><img src="<?php echo get_template_directory_uri()?>/dist/images/before-arrow.svg" alt=""></a>
							<?php $prevthumbnail = get_the_post_thumbnail_url($prevPost->ID, array(200,200) );?>
								<a style="display: contents" href="<?php echo $prevLink ?>">
									<div class="prev-image w-bg-img" style="background-image: url('<?php echo $prevthumbnail ?>')"></div>
									<div class="before-link p-3">
										<?php previous_post_link('%link'," <p>%title</p>", TRUE); ?>
									</div>
								</a>
						</div>
						
						<?php } else{
					    ?>
                        <div class="nav-box previous flex  w-1/2">

                        </div>

                    <?php }

					$nextPost = get_next_post(true);
						if($nextPost): ?>

						<div class="nav-box next flex desktop  w-1/2">
							
							<?php $nextLink = get_permalink(get_adjacent_post(false,'',false));?>
							<?php $nextthumbnail = get_the_post_thumbnail_url($nextPost->ID, array(200,200) );?>

							<div class="after-link p-3">
								<?php next_post_link('%link',"<p>%title</p>", TRUE); ?>
							</div>
							<a style="display: contents" href="<?php echo $nextLink ?>">
								<div class="prev-image w-bg-img" style="background-image: url('<?php echo $nextthumbnail ?>')"></div>
							</a>
							<a class="arrow-link next hidden lg:block" href="<?php echo $nextLink ?>"><img src="<?php echo get_template_directory_uri()?>/dist/images/after-arrow.svg" alt=""></a>
						</div>

							<div class="mobile-nav container w-full flex md:hidden justify-between items-center relative py-4">
								<a class="arrow-link previous" href="<?php echo $prevLink ?>">Previous</a>
								<a class="arrow-link next" href="<?php echo $nextLink ?>">Next</a>
							</div>

                    <?php else: ?>

                            <div class="nav-box next flex w-1/2 hidden lg:flex">

                            </div>

                            <div class="mobile-nav container w-full flex lg:hidden justify-between items-center relative py-3">
                                <a class="arrow-link previous" href="<?php echo $prevLink ?>">Previous</a>
                                <a class="arrow-link next" href="<?php echo $nextLink ?>">Next</a>
                            </div>

                        <?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<section id="contact-block" class="contact-block to-fade-in flex items-stretch justify-center overflow-hidden" style="background-color: #031D51;">
	<div class="container flex flex-col width-control lg:flex-row">
	<div class="w-full lg:w-1/2 image-section" style="background-image: url('<?php echo $contactImage ?>');
    background-size: cover; 
    background-position: center top;
    background-repeat: no-repeat;">
        <div class="placeholder py-10"></div>
    </div>
    <div class="w-full lg:w-1/2 form-section pl-7 py-6 form-width" style="background-color:">
        <div class="w-100">
            <h4><?php echo $contactHeading ?></h4>
            <?php echo do_shortcode($contactShortcode) ?>
        </div>
    </div>
	</div>
</section>
</main>

<?php get_footer();
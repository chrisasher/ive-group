<?php
function theme_customize_register( $wp_customize ) {
	// News Page Settings
	$wp_customize->add_section( 'news_settings' , array(
		'title'      => __( 'News Page Settings', 'theme' ),
		'priority'   => 30,
	) );
	
	$wp_customize->add_setting( 'blog_heading' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'blog_heading', array(
			'label' => __('Heading', 'theme'),
			'section' => 'news_settings',
			'settings' => 'blog_heading',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'description' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'description', array(
			'label' => __('Description', 'theme'),
			'section' => 'news_settings',
			'settings' => 'description',
			'type' => 'textarea',
		)
	);
	
	$wp_customize->add_setting( 'latest_heading' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'latest_heading', array(
			'label' => __('Latest Blog Heading', 'theme'),
			'section' => 'news_settings',
			'settings' => 'latest_heading',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'news_grid_heading' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'news_grid_heading', array(
			'label' => __('News Grid Heading', 'theme'),
			'section' => 'news_settings',
			'settings' => 'news_grid_heading',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'contact_heading' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_heading', array(
			'label' => __('Contact Heading', 'theme'),
			'section' => 'news_settings',
			'settings' => 'contact_heading',
			'type' => 'text',
		)
	);
	
	$wp_customize->add_setting( 'contact_shortcode' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_shortcode', array(
			'label' => __('Contact Section Shortcode', 'theme'),
			'section' => 'news_settings',
			'settings' => 'contact_shortcode',
			'type' => 'text',
		)
	);

	// News Page Header Image
	$wp_customize->add_setting('header_image', array(
	));
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize, 'header_image', array(
			'label' => __('Image', 'theme'),
			'section'   => 'news_settings',
			'settings'  => 'header_image',
		)
	));

	// News Page Contact Image
	$wp_customize->add_setting('contact_image', array(
	));
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize, 'contact_image', array(
			'label' => __('Image', 'theme'),
			'section'   => 'news_settings',
			'settings'  => 'contact_image',
		)
	));

	/*
	 * social links start
	 */
	$wp_customize->add_section( 'theme_social_links' , array(
		'title'      => __( 'Social Links', 'theme' ),
		'priority'   => 30,
	) );
	//twitter
	$wp_customize->add_setting( 'twitter_link' , array(
		'default' => 'http://',
	) );
	// instagram
	$wp_customize->add_setting( 'instagram_link' , array(
		'default' => 'http://',
	) );
	// linkedin
	$wp_customize->add_setting( 'linkedin_link' , array(
		'default' => 'http://',
	) );

	$wp_customize->add_control(
		'twitter_link', array(
			'label' => __('Twitter Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'twitter_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'instagram_link', array(
			'label' => __('Instagram Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'instagram_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'linkedin_link', array(
			'label' => __('Linkedin Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'linkedin_link',
			'type' => 'url',
		)
	);
	/*
	 * social links end
	 */

	/*
	 * Address Info Start
	 */
	$wp_customize->add_section( 'theme_contact_info' , array(
		'title'      => __( 'Contact Info', 'theme' ),
		'priority'   => 30,
	) );


	// Office street address
	$wp_customize->add_setting( 'place' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'place', array(
			'label' => __('Place Name', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'place',
			'type' => 'text',
		)
	);

	// Office street address
	$wp_customize->add_setting( 'office_street_address' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_street_address', array(
			'label' => __('Office Street Address', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_street_address',
			'type' => 'text',
		)
	);

	// Office city
	$wp_customize->add_setting( 'office_city' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_city', array(
			'label' => __('Office City', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_city',
			'type' => 'text',
		)
	);
	// Office state
	$wp_customize->add_setting( 'office_state' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_state', array(
			'label' => __('Office State', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_state',
			'type' => 'text',
		)
	);

	// Office postcode
	$wp_customize->add_setting( 'office_postcode' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_postcode', array(
			'label' => __('Office Postcode', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_postcode',
			'type' => 'text',
		)
	);

	// Office Country
	$wp_customize->add_setting( 'office_country' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_country', array(
			'label' => __('Office Country', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_country',
			'type' => 'text',
		)
	);

	// Contact Phone
	$wp_customize->add_setting( 'contact_phone' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_phone', array(
			'label' => __('Contact Phone', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_phone',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_email' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_email', array(
			'label' => __('Contact Email', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_email',
			'type' => 'text',
		)
	);

	// Copyright Details
	$wp_customize->add_setting( 'copyright_details' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'copyright_details', array(
			'label' => __('Copyright Details', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'copyright_details',
			'type' => 'text',
		)
	);
}
add_action( 'customize_register', 'theme_customize_register' );

// Widgets


function footer_widgets_init_1() {
	register_sidebar( array(
		'name'          => __( 'Footer Menu', 'theme' ),
		'id'            => 'footer-services-1',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-services-widget">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init_1' );

function footer_widgets_init_2() {
	register_sidebar( array(
		'name'          => __( 'Footer Menu 2', 'theme' ),
		'id'            => 'footer-services-2',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-services-widget-logos">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init_2' );

function footer_widgets_init_3() {
	register_sidebar( array(
		'name'          => __( 'Footer Icons', 'theme' ),
		'id'            => 'footer-services-3',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-services-widget-logos">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init_3' );

function footer_legal() {
	register_sidebar( array(
		'name'          => __( 'Footer Legal', 'theme' ),
		'id'            => 'footer-legal',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-legal">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_legal' );
<?php
function get_the_excerpt_max_charlength($id, $charlength) {
	$excerpt = get_the_excerpt($id);
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			$out = mb_substr( $subex, 0, $excut );
		} else {
			$out = $subex;
		}
		$out .= '....';
	} else {
		$out = $excerpt;
	}
	return $out;
}

function print_v($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}

function custom_wp_trim_excerpt($text, $wordcount=500) {
	$raw_excerpt = $text;
	if ( '' == $text ) {
		//Retrieve the post content.
		$text = get_the_content('');
	}
	//Delete all shortcode tags from the content.
	$text = strip_shortcodes( $text );

	$text = apply_filters('the_content', $text);
	$text = str_replace(']]>', ']]&gt;', $text);

	$allowed_tags = ''; /*** MODIFY THIS. Add the allowed HTML tags separated by a comma.***/
	$text = strip_tags($text, $allowed_tags);

	$excerpt_word_count = $wordcount; /*** MODIFY THIS. change the excerpt word count to any integer you like.***/
	$excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);

	$excerpt_end = ''; /*** MODIFY THIS. change the excerpt endind to something else.***/
	$excerpt_more = apply_filters('excerpt_more', '' . $excerpt_end);

	$words = preg_split("/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY);
	if ( count($words) > $excerpt_length ) {
		array_pop($words);
		$text = implode(' ', $words);
		$text = $text . $excerpt_more;
	} else {
		$text = implode(' ', $words);
	}
	return $text;
//	return apply_filters('wp_trim_excerpt', $text, $raw_excerpt);
}

// remove woocommerce styles if ther are there
add_filter( 'woocommerce_enqueue_styles', '__return_false');


// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 3; // 3 products per row
	}
}


// remove image height and width
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
	$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	return $html;
}

//add_filter( 'gform_ajax_spinner_url', 'my_wpcf7_ajax_loader' );
add_filter('wpcf7_ajax_loader', 'my_wpcf7_ajax_loader');
function my_wpcf7_ajax_loader () {
	return  get_template_directory_uri().'/dist/images/wheel.gif';
}

function misha_my_load_more_scripts() {
	if ( is_home() ) {
	global $wp_query; 
 
	// In most cases it is already included on the page and this line can be removed
	// wp_enqueue_script('jquery');
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/plugs/loadmore/myloadmore.js', array('jquery') );
 
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ), // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
	 wp_enqueue_script( 'my_loadmore' );
	}
}
 
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );

function misha_loadmore_ajax_handler(){
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'template-parts/post/content', get_post_format() );
			// for the test purposes comment the line above and uncomment the below one
			//the_title();
 
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}





function get_other_post($order='DESC'){
    // WP_Query arguments
    $args = array(
        'post_type'              => array( 'post' ),
        'post_status'            => array( 'publish' ),
        'nopaging'               => true,
        'posts_per_page'         => 1,
        'order'                  => $order,
        'orderby'                => 'date',
    );

// The Query
    $query = new WP_Query( $args );

// The Loop
    $post = new stdClass();
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
            // do something
            $post->title = get_the_title();
            $post->ID = get_the_ID();
            $post->link = get_the_permalink();
        }
    } else {
        // no posts found
    }

// Restore original Post Data
    wp_reset_postdata();

    return $post;
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}



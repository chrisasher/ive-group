<?php
// Blocks

function oink_theme_setup() {
   
    // Add Align Wide and Full Width options to blocks.
    // This is done by adding the corresponding classname to the block’s wrapper.
    add_theme_support( 'align-wide' );

    // Stop users picking their own colours in the block editor
    add_theme_support( 'disable-custom-colors' );

    // Stop users from changing font sizes
    add_theme_support('disable-custom-font-sizes');

    // Custom Editor Colours
    add_theme_support( 'editor-color-palette', array(
        // array(
        //     'name' => __( 'Colour 1', 'oink' ),
        //     'slug' => 'bg-colour-1',
        //     'color' => '#26193b',
        // ),
        // array(
        //     'name' => __( 'Colour 2', 'oink' ),
        //     'slug' => 'bg-colour-2',
        //     'color' => '#f4b014',
        // ),
        array(
            'name' => __( 'Black', 'oink' ),
            'slug' => 'bg-black',
            'color' => '#000000',
        ),
        array(
            'name' => __( 'White', 'oink' ),
            'slug' => 'bg-white',
            'color' => '#ffffff',
        ),
        array(
            'name' => __( 'Grey', 'oink' ),
            'slug' => 'bg-grey',
            'color' => '#f2f2f2',
        ),

    ) );

    // Add a stylesheet for editing in the Dashboard
    add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
    add_editor_style( 'dashboard.css' );
}

add_action( 'after_setup_theme', 'oink_theme_setup' );


 

//Remove Gutenberg Styling from the frontend
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );




// Add Reusable blocks to menu
add_action( 'admin_menu', 'linked_url' );
function linked_url() {
add_menu_page( 'linked_url', 'Reusable Blocks', 'read', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}

add_action( 'admin_menu' , 'linkedurl_function' );
function linkedurl_function() {
global $menu;
$menu[1][2] = "/wp-admin/edit.php?post_type=wp_block";
}




// Register ACF Blocks
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'Home Header',
        'title'             => __('Home Header'),
        'description'       => __('Home Header Block'),
        'render_template'   => 'partials/blocks/home-header.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Home Below Fold',
        'title'             => __('Home Below Fold'),
        'description'       => __('Home Below Fold Block'),
        'render_template'   => 'partials/blocks/home-below-fold.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Blue Video',
        'title'             => __('Blue Video'),
        'description'       => __('Blue Video Block'),
        'render_template'   => 'partials/blocks/home-blue-video.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Two Column Content',
        'title'             => __('Two Column Content'),
        'description'       => __('Two Column Content Block'),
        'render_template'   => 'partials/blocks/home-2-column.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Services Menu',
        'title'             => __('Services Menu'),
        'description'       => __('Services Menu Block'),
        'render_template'   => 'partials/blocks/home-services-menu.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Latest Blog',
        'title'             => __('Latest Blog'),
        'description'       => __('Latest Blog Block'),
        'render_template'   => 'partials/blocks/home-latest-blogs.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Contact',
        'title'             => __('Contact'),
        'description'       => __('Contact Block'),
        'render_template'   => 'partials/blocks/contact-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'contact-block-two',
        'title'             => __('Contact Two'),
        'description'       => __('Contact Block Two'),
        'render_template'   => 'partials/blocks/contact-block-two.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'contact', 'two' ),
        'enqueue_script'    => get_template_directory_uri() . '/partials/blocks/contact-block-two.js',

    ));

    acf_register_block_type(array(
        'name'              => 'Sitewide Header',
        'title'             => __('Sitewide Header'),
        'description'       => __('Sitewide Header Block'),
        'render_template'   => 'partials/blocks/sitewide-header-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Services Content',
        'title'             => __('Services Content'),
        'description'       => __('Services Content Block'),
        'render_template'   => 'partials/blocks/services-content-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Story Content',
        'title'             => __('Story Content'),
        'description'       => __('Story Content Block'),
        'render_template'   => 'partials/blocks/story.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'About Header',
        'title'             => __('About Header'),
        'description'       => __('About Header Block'),
        'render_template'   => 'partials/blocks/header-video.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'About Page Content',
        'title'             => __('About Page Content Sections'),
        'description'       => __('About Page Content Sections'),
        'render_template'   => 'partials/blocks/about-sections.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'About Page Bottom Content',
        'title'             => __('About Page Bottom Content Section'),
        'description'       => __('About Page Bottom Content Section'),
        'render_template'   => 'partials/blocks/about-bottom.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Header',
        'title'             => __('IVE Care Header'),
        'description'       => __('IVE Care Header'),
        'render_template'   => 'partials/blocks/ive-care-header.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Full Column',
        'title'             => __('IVE Care Full Column'),
        'description'       => __('IVE Care Full Column'),
        'render_template'   => 'partials/blocks/care-single-col.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Switchable Columns',
        'title'             => __('IVE Care Switchable Columns'),
        'description'       => __('IVE Care Switchable Columns'),
        'render_template'   => 'partials/blocks/care-columns.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Single Service Below Fold',
        'title'             => __('IVE Single Service Below Fold'),
        'description'       => __('IVE Single Service Below Fold'),
        'render_template'   => 'partials/blocks/services-below-fold.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'More Services Block',
        'title'             => __('More Services Block'),
        'description'       => __('More Services Block'),
        'render_template'   => 'partials/blocks/more-services.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Sub Services',
        'title'             => __('Sub Services'),
        'description'       => __('Sub Services'),
        'render_template'   => 'partials/blocks/sub-services.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Sub Service Gallery and Content',
        'title'             => __('Sub Service Gallery and Content'),
        'description'       => __('Sub Service Gallery and Content'),
        'render_template'   => 'partials/blocks/image-gallery.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'More Sub Services',
        'title'             => __('More Sub Services'),
        'description'       => __('More Sub Services'),
        'render_template'   => 'partials/blocks/more-sub-services.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Team Members',
        'title'             => __('Team Members'),
        'description'       => __('Team Members'),
        'render_template'   => 'partials/blocks/team-members.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'More About IVE',
        'title'             => __('More About IVE'),
        'description'       => __('More About IVE'),
        'render_template'   => 'partials/blocks/more-about-ive.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Additional Items',
        'title'             => __('IVE Care Additional Items'),
        'description'       => __('IVE Care Additional Items'),
        'render_template'   => 'partials/blocks/ive-care-below-fold.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Waymakers',
        'title'             => __('IVE Care Waymakers'),
        'description'       => __('IVE Care Waymakers'),
        'render_template'   => 'partials/blocks/ive-waymakers.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'IVE Care Waymakers',
        'title'             => __('IVE Care Waymakers'),
        'description'       => __('IVE Care Waymakers'),
        'render_template'   => 'partials/blocks/ive-waymakers.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'About Single Content',
        'title'             => __('About Single Content'),
        'description'       => __('About Single Content'),
        'render_template'   => 'partials/blocks/about-single-content.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Locations Block',
        'title'             => __('Locations Block'),
        'description'       => __('Locations Block'),
        'render_template'   => 'partials/blocks/locations-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Standalone About Items',
        'title'             => __('Standalone About Items'),
        'description'       => __('Standalone About Items'),
        'render_template'   => 'partials/blocks/about-items-standalone.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));
    
    acf_register_block_type(array(
        'name'              => 'Free Text Block',
        'title'             => __('Free Text Block'),
        'description'       => __('Free Text Block'),
        'render_template'   => 'partials/blocks/free-text.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'buttons', 'button' ),
    ));
    
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}

// $sub->post_title
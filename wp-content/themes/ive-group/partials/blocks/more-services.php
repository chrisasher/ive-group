<?php
/**
 * More Services
 */
$image = get_field('image');
$arrow = get_field('inner_arrows');
$heading = get_field('heading');

// Get the post query
$loop = new WP_Query( array(
    'post_type' => 'our_services' ,
    'post_status' => 'publish',
    'orderby' => 'date' ,
    'order' => 'ASC' ,
    'posts_per_page' => -1,
    'post_parent'   => 0,
    'post__not_in' => array( get_the_ID() )
))
?>
<section id="more-services" class="more-services to-fade-in flex items-center justify-center lg:py-7">
    <div class="container flex flex-col lg:flex-row items-start relative">
        <div class="w-full lg:w-1/2 relative more-services-section p-2 lg:p-6 text-white h-full">
            <h3 class="w-full lg:w-1/3"><?php echo $heading ?></h3>
            <div class="services-inner-arrow">
                <img class="" src="<?php echo $arrow ?>" alt="">
            </div>
        </div>
        <div class="w-full lg:w-1/2 more-services-bg" style="background-image: url('<?php echo $image ?>');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;">
        </div>
        <div class="menu-items flex flex-col lg:flex-row absolute bottom-0 right-0 left-0">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <a href="<?php echo get_the_permalink(); ?>">
                    <div class="more-services-item lg:p-4" style="background-color:<?php the_field('services_cpt_background_color', get_the_ID()) ?>; color:<?php the_field('services_cpt_text_color', get_the_ID()) ?> ">
                        <div class="colored-section">
                            <h3 class="lg:w-4/5"><?php echo get_the_title(); ?></h3>
                        </div>
                        <div class="flex transform-arrow">
                            <img class="service-arrow" src="<?php the_field('services_cpt_arrow_image', get_the_ID()) ?>" alt="">
                            <p>Learn More</p>
                        </div>
                    </div>
                </a>
            <?php endwhile; ?>
        </div>
    </div>
</section>
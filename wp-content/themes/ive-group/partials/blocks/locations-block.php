<?php
/**
 * Locations Block
 */

$terms = get_terms(array(
    'taxonomy' => 'area',
    'hide_empty' => false,
    'order' => 'ASC'
));

//print_v($terms);

?>
<section id="locations-block" class="locations-block">
    <div class="container">
        <div class="tab-links py-3 lg:py-5">
            <div class="tab-selection flex">
                <?php foreach($terms as $term){
                    echo '<a class="pb-2" href="#'.$term->slug.'">'.$term->name.'</a>';
                }
                ?>
            </div>
        </div>
        <div class="tab-content-section flex flex-row pb-7 lg:pt-2">
            <?php
                foreach($terms as $term){
                    echo '<div id="'.$term->slug.'" class="tab-content flex flex-row flex-wrap w-full">';
                    $tax_query = array(
                        array(
                            'taxonomy' => 'area',
                            'terms' => $term->term_id,
                        ),
                    );
                    
                    // WP_Query arguments
                    $args = array(
                        'post_type' => array( 'location' ),
                        'posts_per_page' => -1,
                        'order' => 'DESC',
                        'tax_query' => $tax_query
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) {
                        while ( $query->have_posts() ) {
                            $query->the_post();

                            $map_link = 'https://www.google.com/maps/search/'.str_replace(' ', '+', get_field('address', get_the_ID()));

                            ?>
                            <div class="single-location flex flex-col relative">
                                <h4><?php the_title(); ?></h4>
                                <div class="address mt-auto">
                                    <p><?php echo get_field('address', get_the_ID()) ?></p>
                                </div>
                                <img class="absolute bottom-0 right-0 hidden lg:block" src="<?php echo get_template_directory_uri()?>/dist/images/location-spiral.svg" alt="">
                                <div class="links flex flex-col mt-auto">
                                    <a class="phone mb-2 ml-2 mr-auto" href="tel:<?php echo get_field('phone_number', get_the_ID()); ?>"><?php echo get_field('phone_number', get_the_ID()); ?></a>
                                    <a class="location ml-2 mr-auto" href="<?php echo $map_link; ?>" target="_blank">Go to map</a>
                                </div>
                            </div>
                            <?php
                        }
                    } else {
                        // no posts found
                    }

                    // Restore original Post Data
                        wp_reset_postdata();
                        echo '</div>'; // close the single tab
                    }
                    ?>
            </div>
        </div>
</section>
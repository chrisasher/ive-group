<?php
/**
 * Services Story Block
 */
$content = get_field('story_content');
$image = get_field('story_image');
$wayfinders = get_field('wayfinders');
?>
<section id="story-block" class="story-block mb-7 lg:mb-9 pt-3">
    <div class="container flex flex-col lg:flex-row items-center justify-center relative">
        <div class="w-full lg:w-1/3 mx-auto story-content">
            <?php if($content): ?>
                <?php echo $content ?>
            <?php endif; ?>
        </div>
        <div class="w-full lg:w-1/2">
            <?php if($image): ?>
                <img class="w-full" src="<?php echo $image ?>" alt="">
            <?php endif; ?>
        </div>
        <?php if($wayfinders): ?>
            <img class="absolute wayfinders" src="<?php echo $wayfinders ?>" alt="">
        <?php endif; ?>
    </div>
</section>
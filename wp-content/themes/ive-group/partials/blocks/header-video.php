<?php
/**
 * About Header Video Block
 */
$heading = get_field('heading');
$video = get_field('video');
$content = get_field('content');
?>
<section id="about-header-block" class="about-header-block justify-center">
    <div class="container flex flex-wrap lg:flex-row items-center pt-4 pb-4 pb-0 lg:pt-9 lg:pb-8">
        <div class="w-2/5">
            <div class="w-full lg:w-5/6">
                <?php if($heading): ?>
                    <h1 class="sitewide-header"><?php echo $heading ?></h1>
                <?php endif; ?>
                <?php if($content): ?>
                <div class="service-content-section lg:pt-3">
                    <?php echo $content ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
        <div class="w-1/2 image-section ml-auto">
            <div class="about-video relative">
                <?php if($video): ?>
                    <video class="header-video relative" autoplay="autoplay" muted="muted" playsinline>
                        <source src="<?php echo $video ?>" type="video/mp4" />
                        Your browser does not support the video tag.
                        </video>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
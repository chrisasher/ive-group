<?php
/**
 * Dummy More Services
 */
$image = get_field('image');
$arrow = get_field('inner_arrows');
$heading = get_field('heading');
$subHeading = get_field('sub_heading');

$thisId = get_the_id();

$args = array(
    'post_status' => 'publish',
    'post_parent' => wp_get_post_parent_id($thisId)
);

$moreSubServices = get_children($args);
if(isset($moreSubServices) && count($moreSubServices)){
    foreach($moreSubServices as $sub){
        if($sub->ID !== $thisId && get_field('empty_service_class', $sub->ID) !== 'empty-service'){
            // print_v($sub);
        }
    }
}
?>
<section id="more-sub-services" class="more-sub-services to-fade-in pt-4 lg:py-7">
<div class="container">
<h3><?php echo $subHeading ?></h3>
</div>
    <div class="container flex flex-wrap overflow-hidden items-start relative pt-2">
        <?php if(isset($moreSubServices) && count($moreSubServices)): ?>
            <?php foreach($moreSubServices as $sub): ?>
                <?php if($sub->ID !== $thisId && get_field('empty_service_class', $sub->ID) !== 'empty-service'): ?>
                    <div class="w-1/2 overflow-hidden lg:my-3 lg:w-1/4 xl:w-1/4">
                        <a href="<?php echo get_the_permalink($sub->ID); ?>">
                            <div class="zoom-bg">
                                <div class="sub-service-bg cover-image" style="background-image: url('<?php echo get_the_post_thumbnail_url($sub->ID) ?>'); background-posiiton: center;
                                background-size: cover;
                                background-repeat: no-repeat;"></div>
                            </div>
                            <div class="flex sub-title items-start pt-2">
                                <img class="mr-2" src="<?php the_field('services_cpt_arrow_image', $sub->ID) ?>" alt="">
                                <p class="w-2/3"><?php echo $sub->post_title; ?></p>
                            </div>
                        </a>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>
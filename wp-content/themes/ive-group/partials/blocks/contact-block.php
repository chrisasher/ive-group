<?php
/**
 * Contact Block Home
 */
$heading = get_field('contact_title');
$image = get_field('contact_image');
$content = get_field('form_content');
$bgColor = get_field('bg_color');
?>
<section id="contact-block" class="contact-block to-fade-in flex items-stretch justify-center overflow-hidden" style="background-color:<?php echo $bgColor ?>">
    <div class="container width-control flex flex-col lg:flex-row">
        <div class="w-full lg:w-1/2 image-section" style="background-image: url('<?php echo $image ?>');
        background-size: cover; 
        background-position: center top;
        background-repeat: no-repeat;">
            <div class="placeholder py-9 lg:py-10"></div>
        </div>
        <?php if($content): ?>
            <div class="w-full lg:w-1/2 form-section py-5 lg:pl-7 lg:py-6 form-width" style="background-color:<?php echo $bgColor ?>">
                <div class="w-100">
                    <h4><?php echo $heading ?></h4>
                    <?php echo $content ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
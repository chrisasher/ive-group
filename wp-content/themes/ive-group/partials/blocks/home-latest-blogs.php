<?php
/**
 * Home latest blog
 */
$heading = get_field('blog_title');
?>
<section id="latest-blog" class="latest-blog pt-6 lg:py-6 flex items-center justify-center to-fade-in">
    <div class="container relative flex flex-wrap items-center">
        <?php if($heading): ?>
            <div class="blog-heading">
                <h3><?php echo $heading ?></h3>
            </div>
        <?php endif; ?>
        <?php
            $loop = new WP_Query( array(
                'post_type' => 'post' ,
                'post_status' => 'publish',
                'orderby' => 'date' ,
                'order' => 'DESC' ,
                'posts_per_page' => 4
            )
        );
        ?>
        <div class="flex flex-wrap blog-parent">
         <?php $featured_img_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <?php $image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg'; ?>
            <div class="w-full lg:w-1/2 news-block-post pb-2 lg:pb-4 lg:mt-2 lg:m-2">
                <div class="inner flex flex-col lg:flex-row-reverse pt-3">
                    <div class="w-1/2 relative post-link-holder">
                        <span><?php echo get_the_date('d.m.Y'); ?></span>
                        <a href="<?php echo get_the_permalink(); ?>"><p class="news-block-title mb-3 lg:pt-2 w-4/5 lg:w-5/6"><?php echo get_the_title(); ?></p></a>
                        <a class="arrow-effect" href="<?php echo get_the_permalink(); ?>"><svg class="hov-effect blog-arrow" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.14539 0L0 0L0 3.85511L10.363 8.70487L0 13.5983L0 17.4545H3.14297L16 10.9152V6.5313L3.14539 0Z" fill="#F2F2F2"/>
                        </svg></a>
                        <!-- <a class="arrow-effect" href="<?//php echo get_the_permalink(); ?>"><img class="blog-arrow" src="<?//php echo get_template_directory_uri() ?>/dist/images/grey-arrow.svg" alt=""></a> -->
                    </div>
                    <div class="w-1/2 lg:mr-3 zoom-bg">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="cover-image"  style="background-image: url('<?php echo $image ?>');
                            background-size: cover;
                            background-position: center center;
                            background-repeat: no-repeat;"></div>
                        </a>
                    </div>
                </div>
            </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
</section>
<?php
/**
 * IVE Care Full Column Block
 */
$content = get_field('content');
?>
<section id="care-single-col" class="care-single-col flex items-center justify-center">
    <div class="container">
        <div class="w-2/3 mx-auto relative py-7">
            <div class="content-holder">
            <?php if($content): ?>
                <?php echo $content ?>
            <?php endif; ?>
            </div>
        </div>
    </div>
</section>   
<?php
/**
 * IVE Care Header
 */
$arrowImage = get_field('wayfinder_image');
$content = get_field('content');
$heading = get_field('heading');
?>
<section id="ive-care" class="ive-care flex items-center justify-center flex- lg:pt-9 pb-7">
    <div class="container relative">
        <div class="w-2/3">
            <?php if($heading): ?>
                <h1 class="sitewide-header"><?php echo $heading ?></h1>
            <?php endif; ?>
            <?php if($content): ?>
            <div class="service-content-section lg:pt-3 w-4/5">
                <?php echo $content ?>
            </div>
            <?php endif; ?>
        </div>
        <?php if($arrowImage): ?>
            <img class="absolute hidden lg:block" src="<?php echo $arrowImage ?>" alt="">
        <?php endif; ?>
    </div>
</section>
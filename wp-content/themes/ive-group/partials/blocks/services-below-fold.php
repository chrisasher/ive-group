<?php
/**
 * IVE Care Columns
 */
$image = get_field('image');
$video = get_field('video');
$arrow = get_field('arrow_image');
$poster = get_field('preview_poster_image');
$content = get_field('content');

if($video) {
    $class = 'service-main-video';
} else {
    $class = 'service-image';
}
?>
<section id="services-below-fold" class="services-below-fold justify-center lg:pt-7 lg:pb-5">
    <div class="container flex flex-wrap justify-center items-start">
        <div class="w-full lg:w-1/2 overflow-hidden">
            <div class="<?php echo $class ?> flex items-center justify-center">
                <?php if($video): ?>
                    <video id="serv-video" class="service-video" src="<?php echo $video ?>" autoplay="autoplay" loop="loop" muted="muted" playsinline 
                    poster="<?php echo $poster ?>">
                        Your browser does not support the video tag.
                    </video>
                    <img class="absolute cursor-pointer play-button" src="<?php echo get_template_directory_uri() ?>/img/play_button.svg" alt="">
                <?php else: ?>
                    <img class="w-full" src="<?php echo $image ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="services-single-arrow">
                <img class="mx-auto mt-4" src="<?php echo $arrow ?>" alt="">
            </div>
        </div>
        <div class="w-full lg:w-1/2 py-2 lg:py-0">
            <div class="w-full lg:w-2/3 lg:mx-auto bf-holder">
                <?php echo $content ?>
            </div>
        </div>
    </div>
    <div class="modal">
        <div class="video-holder">
            <img class="close-button pointer-events-auto cursor-pointer absolute right-0" src="<?php echo get_template_directory_uri() ?>/dist/images/close.svg" alt="">
            <?php if($video): ?>
                <video id="modal" class="relative object-cover object-center w-full md:h-auto" autoplay controls>
                    <source src="<?php echo $video ?>" type="video/mp4" />
                    Your browser does not support the video tag.
                </video>
            <?php endif; ?>
        </div>
    </div>
</section>

<script>

    const modal = document.querySelector('.modal');
    const playButton = document.querySelector('.play-button');
    const video = document.querySelector('.video-holder');
    const closeButton = document.querySelector('.close-button');

    playButton.addEventListener('click', () => {
        document.querySelector('#modal').muted = false;
		modal.classList.add('open');
		video.classList.add('open');
		document.querySelector('#modal').play();
		document.querySelector('.service-video').pause();
	})
	
	closeButton.addEventListener('click', (e) => {
		modal.classList.remove('open');
		video.classList.remove('open');
		document.querySelector('.service-video').play();
		document.querySelector('#modal').pause();
	})
	
	modal.addEventListener('click', (e) => {
		if(e.target.classList.contains('modal')) {
			modal.classList.remove('open');
			video.classList.remove('open');
			document.querySelector('#modal').pause();
			document.querySelector('.service-video').play();
		}
    });

</script>
<?php
/**
 * Home services menu
 */
$services = get_field('services_menu');
// Get the post query
$loop = new WP_Query( array(
    'post_type' => 'our_services' ,
    'post_status' => 'publish',
    'orderby' => 'date' ,
    'post_parent' => '0',
    'order' => 'ASC' ,
    'posts_per_page' => 4
));
$colour_theme = get_field('arrow_animation_direction');
?>
<section id="services-menu" class="services-menu py-3 lg:py-7 flex items-center to-fade-in">
    <div class="container relative flex flex-wrap items-center">
        <div class="service-grid flex flex-wrap">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="w-1/2 service-item">
                        <a class="service-link flex" href="<?php echo get_the_permalink(); ?>">
                            <div class="w-2/3 relative color-section mr-1" style="background-color:<?php the_field('services_cpt_background_color', get_the_ID()) ?>; color:<?php the_field('services_cpt_text_color', get_the_ID()) ?>">
                                <h3 class="lg:w-2/3"><?php echo get_the_title(); ?></h3>
                                <span>
                                    <img class="service-arrow <?php the_field('arrow_animation_direction', get_the_ID()) ?>" src="<?php the_field('services_cpt_arrow_image', get_the_ID()) ?>" alt="">
                                </span>
                            </div>
                            <div class="w-1/3 mr-1 w-bg-img image-zoom" style="background-image:url(<?php the_field('smaller_image', get_the_ID()) ?>)">
                                <img class="object-contain hidden lg:block desktop" src="<?php the_field('smaller_image', get_the_ID()) ?>" alt="">
                            </div>
                        </a>
                    </div>
            <?php endwhile; ?>
        </div>
        <img class="absolute services-image" src="<?php echo get_template_directory_uri(); ?>/dist/images/services.svg" alt="">
    </div>
</section>
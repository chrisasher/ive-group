<?php
/**
 * About Bottom Content Block
 */
$arrowImage = get_field('arrows');
$imageContent = get_field('image');
$content = get_field('content');
$title = get_field('title');
$link = get_field('link');

// $arrowImage = get_field('arrow_image');
$content = get_field('content');
$image = get_field('content_image');

$field = get_field_object('column_align');
$value = $field['value'];
$label = $field['choices'][ $value ];
$class = '';
$imgClass = '';
$colClass = '';

if($label == 'Yes') {
    $imageClass = 'pos-right right-0';
    $class = 'lg:flex-row-reverse ive-care-adjust';
    $firstSection = 'add-padding';
} else {
    $imageClass = 'pos-left';
    $class = 'lg:flex-row no-adjustment';
    $firstSection = '';
}
?>
<section id="about-bottom-content" class="ive-switchable-columns about-bottom-content lg:py-7">
    <div class="container flex items-center justify-center">
        <div class="mx-auto relative lg:pt-3 flex flex-wrap <?php echo $class ?>">
            <div class="w-full lg:w-2/5 mx-auto lg:pt-4 <?php echo $firstSection; ?>">
                <div class="content-holder pb-3 lg:pb-0">
                    <?php if($title): ?>
                        <h3><?php echo $title ?></h3>
                    <?php endif; ?>
                    <div class="border"></div>
                    <?php if($content): ?>
                        <div class="para">
                            <?php echo $content ?>
                        </div>
                    <?php endif; ?>
                    <?php if($link): ?>
                        <a href="<?php echo $link ?>" class="read-more-link">Learn More</a>
                    <?php endif; ?>
                    <?php if($arrowImage): ?>
                        <img class="<?php echo $imageClass; ?>" src="<?php echo $arrowImage ?>" alt="">
                    <?php endif; ?>
                </div>
            </div>
            <div class="w-full lg:w-1/2">
                <?php if($imageContent): ?>
                    <img class="w-full ml-auto" src="<?php echo $imageContent ?>" alt="">
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>   
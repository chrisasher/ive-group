<?php
/**
 * About Single Content
 */
$wayfinders = get_field('wayfinder');
?>
<section id="single-about-content" class="single-about-content to-fade-in pt-3 lg:pt-3 lg:pb-7 relative">
    <?php if($wayfinders): ?>
        <img class="absolute right-0 hidden lg:block" src="<?php echo $wayfinders ?>" alt="">
    <?php endif; ?>
    <div class="container">
        <div class="single-about-items-grid flex flex-wrap">
            <?php if( have_rows('add_content') ): ?>
                <?php while( have_rows('add_content') ): the_row(); ?>
                    <div class="top-width w-full lg:w-1/2">
                        <div class="single-about-item">
                            <span><?php the_sub_field('heading'); ?></span>
                            <p class="pt-2"><?php the_sub_field('content'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
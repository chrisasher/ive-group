<?php
/**
 * Home Blue Vide Section
 */
$video = get_field('video');
$afterVideo = get_field('after_video');
$mazeBG = get_field('maze_bg');
?>
<section id="maze-video" class="maze-video flex justify-center lg:py-0" style="background-color: <?php echo $mazeBG ?>">
    <div class="relative">
        <?php if($video): ?>
            <video id="marketing-video" loop="loop" muted="muted" playsinline>
                <source src="<?php echo $video ?>" type="video/mp4" />
                Your browser does not support the video tag.
            </video>
        <?php endif; ?>
        <?php if($video): ?>
            <img src="<?php echo $afterVideo ?>" alt="">
        <?php endif; ?>
    </div>
</section>
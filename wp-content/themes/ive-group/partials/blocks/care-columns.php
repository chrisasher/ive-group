<?php
/**
 * IVE Care Columns
 */
$arrowImage = get_field('arrow_image');
$content = get_field('content');
$image = get_field('content_image');

$field = get_field_object('column_align');
$value = $field['value'];
$label = $field['choices'][ $value ];
$class = '';
$imgClass = '';
$colClass = '';

if($label == 'Yes') {
    $class = 'position-arrows-left flex-row-reverse';
    $colClass = 'mx-auto lg:pt-8';
} else {
    $class = 'position-arrows-right flex-row';
    $colClass = 'mx-auto lg:pt-7';
}
?>
<section id="ive-switchable-cols" class="ive-switchable-cols py-5">
    <div class="container relative flex items-center justify-center <?php echo $class ?>">
        <div class="w-1/2">
            <div class="w-2/3 <?php echo $colClass ?>">
                <?php if($content): ?>
                    <div class="service-content-section">
                        <?php echo $content ?>
                    </div>
                <?php endif; ?>

                    <?php if( have_rows('button') ): ?>
                    <?php while( have_rows('button') ): the_row(); 

                        // Get sub field values.
                        $text = get_sub_field('text');
                        $link = get_sub_field('link');
                        ?>

                    <div class="learn-more">
                        <a href="<?php echo $link ?>" class="read-more-link"><?php echo $text ?></a>
                    </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <div class="w-1/2">
            <?php if($image): ?>
                <img class="w-full" src="<?php echo $image ?>" alt="">
            <?php endif; ?>
        </div>

        <div class="arrows">
            <?php if($arrowImage): ?>
                <img class="absolute" src="<?php echo $arrowImage ?>" alt="">
            <?php endif; ?>
        </div>
    </div>
</section>
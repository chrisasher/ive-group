<?php
/**
 * Services Content Block
 */
$arrowImage = get_field('design_arrows');
$bg = get_field('services_cpt_background_color');
$description = get_field('services_description', get_the_ID());

// Get the post query
$loop = new WP_Query( array(
    'post_type' => 'our_services' ,
    'post_status' => 'publish',
    'orderby' => 'date' ,
    'post_parent' => 0,
    'order' => 'ASC' ,
    'posts_per_page' => 4
))
?>
<section id="service-content" class="services-content pt-5 lg:py-7 relative">
    <?php if($arrowImage): ?>
        <img class="lg:pt-4 absolute top-0 mx-auto text-center right-0 left-0" src="<?php echo $arrowImage ?>" alt="">
    <?php endif; ?>
    <div class="container flex flex-wrap">
        <div class="service-content-grid flex flex-wrap pt-5 lg:pt-5">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="w-1/2 <?php echo $textColor ?>" style="background-color:<?php the_field('services_cpt_background_color', get_the_ID()) ?>;">
                    <a class="flex flex-col service-content-item pl-6 pt-5 pr-6 relative" style="color:<?php the_field('services_cpt_text_color', get_the_ID()) ?>" href="<?php echo get_the_permalink(); ?>">
                        <div class="colored-section">
                            <h3 class="lg:w-4/5"><?php echo get_the_title(); ?></h3>
                            <div class="services-content-section">
                                <?php the_field('services_description', get_the_ID()) ?>
                            </div>
                            <span>
                                <img class="service-arrow btn-bottom" src="<?php the_field('services_cpt_arrow_image', get_the_ID()) ?>" alt="">
                            </span>
                        </div>
                        <div class="absolute-holder">
                            <div class="image-container">
                                <img class="service-content-image" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                            </div>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
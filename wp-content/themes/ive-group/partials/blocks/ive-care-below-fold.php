<?php
/**
 * Executive Team Members Block
 */
?>
<section id="sub-services" class="sub-services to-fade-in ive-care-informational-items flex items-center justify-center lg:pt-7 relative">
    <div class="container flex items-start relative">
        <div class="sub-service-items flex flex-wrap">
            <?php if( have_rows('add_items') ): ?>
                <?php while( have_rows('add_items') ): the_row(); ?>
                    
                    <?php $isPlaceholder = get_sub_field('is_placeholder');
                        if($isPlaceholder) {
                            $class = 'placeholder-item';
                        } else {
                            $class = '';
                        }
                    ?>

                    <div class="sub-services-item w-full lg:w-1/3 pb-4 lg:pb-7 <?php echo $class; ?>">
                        
                        <div class="flex transform-arrow pt-3 lg:pt-0 mb-2">
                            <h3 class="pl-2"><?php the_sub_field('title') ?></h3>
                        </div>
                    
                        <div class="image-section">
                            <img class="w-full" src="<?php the_sub_field('image') ?>" alt="">
                        </div>

                        <div class="sub-services-content pt-3">
                            <?php the_sub_field('description') ?>
                        </div>

                        <?php if( have_rows('logos') ): ?>
                            <div class="logos flex pt-2 lg:pt-4 items-start">
                                <?php while( have_rows('logos') ): the_row(); ?>
                                    <img class="w-auto mr-3" src="<?php the_sub_field('logo') ?>" alt="">
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>

                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php

/**
 * Home Header Block
 */

$brandBlue = get_field('brand_blue');
$brandLightBlue = get_field('brand_blue_lighter');
$brandArrows = get_field('brand_arrows');
$videoPlay = get_field('image_play');
$fullVideo = get_field('full_video');
$playButton = get_field('play_button');
$imageOne = get_field('image_one');
$imageTwo = get_field('image_two');
$imageThree = get_field('image_three');
$whiteLogo = get_field('white_logo');
$waypointArrows = get_field('waypoint_arrows')
?>

<section id="home-header" class="home-header flex justify-center">
    <div class="w-1/2">
        <div class="flex flex-wrap">
            <div class="w-1/2 part-one">
                <div class="light-blue w-full lg:w-1/2 relative overflow-hidden">
                    <?php if($imageOne): ?>
                        <img src="<?php echo $imageOne ?>" alt="">
                    <?php endif; ?>
                    <!-- <?php if($brandArrows): ?>
                        <img class="mx-auto" src="<?php echo $brandArrows ?>" alt="">
                    <?php endif; ?> -->
                </div>
            </div>
            <div class="w-1/2 part-two flex items-center justify-center relative overflow-hidden">
                <div class="image-play relative">
                    <?php if($videoPlay): ?>
                        <video class="onpage-video relative" data-object-fit="cover" autoplay="autoplay" loop="loop" muted="muted"
                        poster="<?php echo get_template_directory_uri() ?>/img/poster-image.jpg" playsinline>
                            <source src="<?php echo $videoPlay ?>" type="video/mp4" />
                            Your browser does not support the video tag.
                            </video>
                    <?php endif; ?>
                </div>
                <?php if($playButton): ?>
                    <img class="play-button cursor-pointer" src="<?php echo $playButton ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="w-1/2 part-three">
                 <?php if($imageTwo): ?>
                    <img src="<?php echo $imageTwo ?>" alt="">
                <?php endif; ?>
            </div>
            <div class="w-1/2 part-four">
                <?php if($imageThree): ?>
                    <img src="<?php echo $imageThree ?>" alt="">
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="w-1/2 white-logo flex flex-col justify-center items-center relative" style="background-color: <?php if($brandBlue) echo $brandBlue ?>">
        <?php if($whiteLogo): ?>
            <img src="<?php echo $whiteLogo ?>" alt="">
        <?php endif; ?>
        <div class="waypoint-arrows">
            <?php if($waypointArrows): ?>
                <img src="<?php echo $waypointArrows ?>" alt="">
            <?php endif; ?>
        </div>
    </div>
    <div class="modal">
        <div class="video-holder">
            <img class="close-button pointer-events-auto cursor-pointer absolute right-0" src="<?php echo get_template_directory_uri() ?>/dist/images/close.svg" alt="">
            <?php if($videoPlay): ?>
                <video id="video-modal" class="relative object-cover object-center w-full md:h-auto" autoplay controls>
                    <source src="https://www.ivegroup.com.au/IVE_master.mp4" type="video/mp4" />
                    Your browser does not support the video tag.
                </video>
            <?php endif; ?>
        </div>
    </div>
</section>
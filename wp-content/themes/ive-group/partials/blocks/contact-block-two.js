jQuery(document).ready(function ($) {
    $("input, textarea").focus(function () {
        $(this).parent().addClass('underline-grow');
    });
    $("input, textarea").focusout(function () {
        $(this).parent().removeClass('underline-grow');
    });
    $('select').on('change', function () {

        if ($("select").val() !== "") {
            $(this).addClass('floatit');
        } else {
            $(this).removeClass('floatit');
        }
    });

    if($('#choice').checked){
        $('.wpcf7-submit').removeAttr('disabled');
        console.log('checked')
    } else{
        $('.wpcf7-submit').attr('disabled','disabled');
        console.log('unchecked')
    }

    $('#choice').change(function()  {
        console.log('hi');
        if(this.checked){
            $('.wpcf7-submit').removeAttr('disabled');
            console.log('checked')
        } else{
            $('.wpcf7-submit').attr('disabled','disabled');
            console.log('unchecked')
        }
    });
});
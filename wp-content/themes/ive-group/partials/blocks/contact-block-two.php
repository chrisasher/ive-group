<?php
/**
 * Contact Block Two
 */
$heading = get_field('contact_title');
$content = get_field('form_content');
?>
<section class="contact-block-two">
        <?php if($content): ?>
            <div class="flex flex-row py-7 container mx-auto">
                <div class="w-2/3 mx-auto">
                    <?php if($heading) : ?>
                        <h4><?php echo $heading ?></h4>
                    <?php endif; ?>
                    <?php echo $content ?>
                </div>
            </div>
        <?php endif; ?>
</section>
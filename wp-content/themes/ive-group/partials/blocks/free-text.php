<?php
/**
 * Free text
 */
$content = get_field('block');
?>
<section id="story-block" class="story-block mb-7 lg:mb-9 pt-3">
    <div class="container flex flex-col lg:flex-row items-center justify-center relative">
        <div class="w-full lg:w-1/3 mx-auto story-content">
            <?php if($content): ?>
                <?php echo $content ?>
            <?php endif; ?>
        </div>
    </div>
</section>
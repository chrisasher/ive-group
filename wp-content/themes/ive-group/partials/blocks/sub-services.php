<?php
/**
 * Sub Services
 */
$image = get_field('image');
$emptyService = get_field('empty_service_class', get_the_ID());

// Get the post query
$loop = new WP_Query( array(
    'post_type' => 'our_services' ,
    'post_status' => 'publish',
    'orderby' => 'date' ,
    'order' => 'ASC' ,
    'post_parent' => get_the_ID(),
    'posts_per_page' => -1
))
?>
<div class="container hidden lg:block">
    <div class="decorative-border"></div>
</div>
<section id="sub-services" class="sub-services to-fade-in flex items-center justify-center lg:pt-7 relative">
    <?php if($image): ?>
    <div class="decorative-holder">
        <img class="absolute right-0" src="<?php echo $image ?>" alt="">
    </div>
    <?php endif; ?>
    <div class="container flex items-start relative">
        <div class="sub-service-items flex flex-wrap">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <div class="sub-services-item w-full lg:w-1/3 <?php the_field('empty_service_class', get_the_ID()) ?>">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="flex transform-arrow pt-3 lg:pt-0 mb-2">
                                <img class="service-arrow" src="<?php the_field('services_cpt_arrow_image', get_the_ID()) ?>" alt="">
                                <h3 class="lg:w-4/5 pl-2"><?php echo get_the_title(); ?></h3>
                            </div>
                        </a>
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="image-section">
                                <img class="w-full" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                            </div>
                        </a>
                        <div class="sub-services-content pt-3">
                            <?php the_field('services_description', get_the_ID()) ?>
                        </div>
                    </div>
            <?php endwhile; ?>
        </div>
    </div>
</section>
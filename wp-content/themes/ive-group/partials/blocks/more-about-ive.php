<?php
/**
 * More About IVE
 */
$image = get_field('image');
$arrow = get_field('inner_arrows');
$heading = get_field('heading');

$thisId = get_the_id();

$args = array(
    'post_status' => 'publish',
    'order' => 'ASC' ,
    'post_parent' => wp_get_post_parent_id($thisId)
);

$moreSubServices = get_children($args);
if(isset($moreSubServices) && count($moreSubServices)){
    foreach($moreSubServices as $sub){
        if($sub->ID !== $thisId){
            // print_v($sub);
        }
    }
}
?>
<section id="more-about-ive" class="more-services to-fade-in more-ive-section flex items-center justify-center lg:pb-10">
    <div class="container flex flex-col lg:flex-row items-start relative">
        <div class="w-1/2 relative more-services-section p-2 lg:p-6 text-white h-full">
            <h3 class="w-full lg:w-3/5"><?php echo $heading ?></h3>
            <div class="services-inner-arrow">
                <img class="" src="<?php echo $arrow ?>" alt="">
            </div>
        </div>
        <div class="w-1/2 more-services-bg" style="background-image: url('<?php echo $image ?>');
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center center;">
        </div>
        <div class="menu-items flex flex-col lg:flex-row absolute bottom-0 right-0 left-0">
            <?php if(isset($moreSubServices) && count($moreSubServices)): ?>
                <?php foreach($moreSubServices as $sub): ?>
                    <?php if($sub->ID !== $thisId): ?>
                        <a href="<?php echo get_the_permalink($sub->ID); ?>">
                            <div class="more-services-item lg:p-4">
                                <div class="colored-section">
                                    <h3 class="lg:w-full"><?php echo $sub->post_title; ?></h3>
                                </div>
                                <div class="flex transform-arrow">
                                    <p>Learn More</p>
                                    <svg class="hov-effect blog-arrow block lg:hidden" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.14539 0L0 0L0 3.85511L10.363 8.70487L0 13.5983L0 17.4545H3.14297L16 10.9152V6.5313L3.14539 0Z" fill="#F2F2F2"/>
                        </svg>
                                </div>
                            </div>
                        </a>
                        <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
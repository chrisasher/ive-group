<?php

/**
 * Home Below Fold
 */
$content = get_field('below_fold_content');
$moreText = get_field('more_text');
$learnMore = get_field('read_more_link');
$sectionImage = get_field('section_image');
$turquoiseArrow = get_field('section_arrow');
$heading = get_field('bf_heading');
?>
<div class="scroll"></div>
<section id="below-fold" class="below-fold flex justify-center py-5 lg:py-7">
    <div class="container flex relative">
        <?php if($content): ?>
        <div class="w-2/5 ml-auto lg:pr-8 lg:pt-7">
            <h2><?php echo $heading ?></h2>
            <?php echo $content ?>
            <?php if( have_rows('letters')): ?>
                <div class="letters">
                <?php while( have_rows('letters') ): the_row(); ?> 
                    <?php $i++ ?>
                        <img class="letter-<?php echo $i ?> absolute" src="<?php the_sub_field('letter') ?>" alt="">
                    <?php endwhile; ?>
                </div>   
            <?php endif; ?>
            <div class="learn-more">
               <a href="<?php echo $learnMore ?>" class="through-link"><?php echo $moreText ?></a>
            </div>
        </div>
        <?php endif; ?>
        <?php if($sectionImage): ?>
            <div class="w-1/2 relative">
                <div class="section-image img-wrapper box">
                    <img class="scroll-img" src="<?php echo $sectionImage ?>" alt="">
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
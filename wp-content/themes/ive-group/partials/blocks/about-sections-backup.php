<?php
/**
 * About Content Block
 */
$arrowImage = get_field('decorative_arrows');

?>
<section id="service-content" class="services-content py-7 relative">
    <?php if($arrowImage): ?>
        <img class="pt-4 absolute top-0 mx-auto text-center right-0 left-0" src="<?php echo $arrowImage ?>" alt="">
    <?php endif; ?>
    <div class="container flex flex-wrap">
        <div class="service-content-grid flex flex-wrap pt-5">
            <?php if( have_rows('section_content') ): ?>
                <?php while( have_rows('section_content') ): the_row(); ?>
                    <a href="<?php the_sub_field('link') ?>">
                        <div class="w-1/2 flex flex-col service-content-item pl-6 pt-5 pr-6 relative <?php echo $textColor ?>" style="background-color:<?php the_sub_field('background_color'); ?>; color:<?php the_sub_field('text_colour'); ?> ">
                            <div class="colored-section">
                                <div class="services-content-section">
                                    <?php the_sub_field('content') ?>
                                </div>
                                <img class="service-arrow" src="<?php the_sub_field('arrow_image') ?>" alt="">
                            </div>
                            <div class="image-container">
                                <img class="service-content-image" src="<?php the_sub_field('image') ?>" alt="">
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>

<div class="container flex flex-wrap">
        <div class="service-content-grid flex flex-wrap pt-5">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <a href="<?php echo get_the_permalink(); ?>">
                    <div class="w-1/2 flex flex-col service-content-item pl-6 pt-5 pr-6 relative <?php echo $textColor ?>" style="background-color:<?php the_field('services_cpt_background_color', get_the_ID()) ?>; color:<?php the_field('services_cpt_text_color', get_the_ID()) ?> ">
                        <div class="colored-section">
                            <h3 class="lg:w-4/5"><?php echo get_the_title(); ?></h3>
                            <div class="services-content-section">
                                <?php the_field('services_description', get_the_ID()) ?>
                            </div>
                            <img class="service-arrow" src="<?php the_field('services_cpt_arrow_image', get_the_ID()) ?>" alt="">
                        </div>
                        <div class="image-container">
                            <img class="service-content-image" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                        </div>
                    </div>
                </a>
            <?php endwhile; ?>
        </div>
    </div>
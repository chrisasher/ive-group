<?php
/**
 * Executive Team Members Block
 */
$loop = new WP_Query( array(
    'post_type' => 'team_members' ,
    'post_status' => 'publish',
    'order' => 'ASC' ,
    'tax_query' => array(
        array(
            'taxonomy' => 'team_category',
            'field'    => 'slug',
            'terms'    => 'first-level-executives'
        ),
    ),
));
$second = new WP_Query( array(
    'post_type' => 'team_members' ,
    'post_status' => 'publish',
    'order' => 'ASC' ,
    'tax_query' => array(
        array(
            'taxonomy' => 'team_category',
            'field'    => 'slug',
            'terms'    => 'second-level-executives'
        ),
    ),
));
$image = get_field('waymakers');
?>
<section id="executive-team" class="executive-team lg:pb-7 lg:pt-8 relative">
    <?php if($image): ?>
        <div class="decorative-holder hidden lg:block">
            <img class="absolute right-0" src="<?php echo $image ?>" alt="">
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="first-level-execs flex-wrap lg:flex xl:flex-no-wrap">
            <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="w-full executive">
                    <?php
                        $placeholder = get_field('placeholder', get_the_ID());
                        if($placeholder): ?>
                        <div class="exec-bg ive-placeholder" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/placeholder.jpg'); background-repeat: no-repeat; background-size: cover; background-position: center center;"></div>
                        <!-- <img class="ive-placeholder w-full ml-auto" src="<?php echo get_template_directory_uri(); ?>/dist/images/placeholder.jpg" alt=""> -->
                    <?php else: ?>
                        <div class="exec-image">
                            <h3 class="lg:w-4/5"><?php echo get_the_title(); ?></h3>
                            <div class="exec-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"></div>
                            <!-- <img class="w-full ml-auto" src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""> -->
                            <span><?php the_field('role', get_the_ID()) ?></span>
                            <p class="w-4/5 pt-1"><?php the_field('department', get_the_ID()) ?></p>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>        
        </div>
        <div class="container hidden lg:block px-0 py-5">
            <div class="decorative-border"></div>
        </div>
        <div class="second-level-execs flex-wrap lg:flex xl:flex-no-wrap pt-3">
            <?php while ( $second->have_posts() ) : $second->the_post(); ?>
                <div class="w-full executive">
                    <div class="exec-image">
                        <h3 class="lg:w-4/5"><?php echo get_the_title(); ?></h3>
                        <div class="exec-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>'); background-repeat: no-repeat; background-size: cover; background-position: center center;"></div>
                        <!-- <img class="w-full ml-auto" src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""> -->
                        <span><?php the_field('role', get_the_ID()) ?></span>
                        <p class="w-4/5 pt-1"><?php the_field('department', get_the_ID()) ?></p>
                    </div>
                </div>
            <?php endwhile; ?>   
        </div>
    </div>
</section>
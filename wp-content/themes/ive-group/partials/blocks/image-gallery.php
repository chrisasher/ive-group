<?php
/**
 * Image Gallery
 */
$field = get_field_object( 'number_of_columns' );
$value = $field['value'];
$label = $field['choices'][ $value ];

$class = '';
if($label == 'One Column') {
    $class = 'gal-1-col';
} elseif ($label == 'Two Column') {
    $class = 'gal-2-col';
} elseif ($label == 'Three Column') {
    $class = 'gal-3-col';
}

$images = get_field('gallery');
$description = get_field('description');

if($description) {
    $pclass = 'pt-5 lg:pt-6';
} else {
    $pclass = '';
}

?>
<section id="services-below-fold" class="services-below-fold flex items-center justify-center pt-1 lg:pt-7 lg:pb-5">
    <div id="gallery-container" class="container flex items-start">
        <div class="w-full lg:w-1/2 relative">
            <!-- Popup gallery -->
            <div class="gal-sticky">
                <div id="gallery" class="gallery flex flex-wrap">
                    <?php if( have_rows('gallery') ): ?>
                        <?php while( have_rows('gallery') ): the_row();
                        $isPlaceholder = get_sub_field('is_placeholder'); ?>
                            <div class="gallery-item square <?php echo $class; ?> w-bg-img" style="background-image: url('<?php the_sub_field('image') ?>');">
                                <?php if(!$isPlaceholder){ ?>
                                    <a href="<?php the_sub_field('image') ?>" class="overlay" title="<?php the_sub_field('image_caption'); ?>"></a>
                                <?php   } ?>
                            </div>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <!-- Popup gallery end -->
        </div>
        
        <div class="w-full lg:w-1/2">
            <div class="w-full lg:w-3/4 mx-auto bf-holder single-sub-service">
                <?php if($description): ?>
                    <span><?php echo $description ?></span>
                <?php endif; ?>
                <div class="service-information <?php echo $pclass ?>">
                    <?php if( have_rows('service_information') ): ?>
                        <?php while( have_rows('service_information') ): the_row(); ?>
                            <div class="information-item">
                                <?php if(get_sub_field('info_title')): ?>
                                    <p class="service-title"><?php the_sub_field('info_title'); ?></p>
                                <?php endif; ?>
                                <div class="service-content"><?php the_sub_field('info_description'); ?></div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
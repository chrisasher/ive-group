<?php
/**
 * About Content Block
 */
$arrowImage = get_field('decorative_arrows');
?>
<section id="service-content" class="services-content py-7 relative">
    <?php if($arrowImage): ?>
        <img class="pt-4 absolute top-0 mx-auto text-center right-0 left-0" src="<?php echo $arrowImage ?>" alt="">
    <?php endif; ?>
    <div class="container flex flex-wrap">
        <div class="service-content-grid flex flex-wrap pt-5">
            <?php if( have_rows('section_content') ): ?>
                <?php while( have_rows('section_content') ): the_row(); ?>
                        <div class="w-1/2 <?php echo $textColor ?>" style="background-color:<?php the_sub_field('background_color'); ?>;">
                            <a class="flex flex-col service-content-item pl-6 pt-5 pr-6 relative" style="color:<?php the_sub_field('text_colour'); ?>" href="<?php the_sub_field('link') ?>">
                            <div class="colored-section">
                                <div class="services-content-section">
                                    <?php the_sub_field('content') ?>
                                </div>
                                <span>
                                    <img class="service-arrow btn-bottom" src="<?php the_sub_field('arrow_image') ?>" alt="">
                                </span>
                            </div>
                            <div class="absolute-holder">
                                <div class="image-container">
                                    <img class="service-content-image" src="<?php the_sub_field('image') ?>" alt="">
                                </div>
                            </div>
                            </a>
                        </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php
/**
 * Home columns
 */
$image = get_field('col_image');
$content = get_field('col_content');
$link = get_field('col_link');
$linkText = get_field('col_text');
?>
<section id="two-column" class="two-column py-3 lg:pt-7 lg:pb-0 justify-center">
    <div class="container relative flex items-center">
        <div class="w-1/2">
            <?php if($image): ?>
                <img class="mx-auto gif-animate" src="<?php echo $image ?>" alt="">
            <?php endif; ?>
        </div>
        <div class="w-1/2">
            <div class="w-3/5 mx-auto fade to-fade-in">
                <?php if($content): ?>
                    <?php echo $content ?>
                <?php endif; ?>
                <?php if($link): ?>
                    <a class="read-more-link" href="<?php echo $link ?>"><?php echo $linkText ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php
/**
 * Header Block
 */
$heading = get_field('header_block_title');
$image = get_field('header_block_image');
$content = get_field('header_block_content');

$field = get_field_object( 'header_bg_color' );
$value = $field['value'];
$label = $field['choices'][ $value ];

$class = '';
$textColor = '';
$colorDetect = '';

if($label == 'Dark Blue'){
    $class = 'underline-color-turquoise';
    $textColor = 'text-white';
    $colorDetect = 'darkblue-block';
} elseif ($label == 'Light Blue') {
    $class = 'underline-color-dark-blue';
    $textColor = 'text-blue';
    $colorDetect = 'lightblue-block';
} elseif ($label == 'Blue') {
    $class = 'underline-color-white';
    $textColor = 'text-white';
    $colorDetect = 'blue-block';
} elseif ($label == 'Turquoise') {
    $class = 'underline-color-dark-blue';
    $textColor = 'text-blue';
    $colorDetect = 'turquoise-block';
} elseif ($label == 'Purple') {
    $class = 'underline-color-white';
    $textColor = 'text-white'; 
    $colorDetect = 'purple-block';
} elseif ($label == 'Tints Blue') {
    $class = 'underline-color-dark-blue';
    $textColor = 'text-blue';
    $colorDetect = 'lightblue-block';
}
?>
<section id="header-content-block" class="header-content-block <?php echo $textColor?> <?php echo $colorDetect ?>" style="background-color:<?php echo esc_attr($value); ?>">
    <div class="container flex flex-col lg:flex-row items-center justify-center py-5 pb-0 lg:pt-9 lg:pb-8">
        <?php if($content): ?>
            <div class="w-full lg:w-2/5">
                <div class="w-full">
                    <h1 class="sitewide-header mb-4 w-full lg:w-4/5 <?php echo $class ?>"><?php echo $heading ?></h1>
                    <div class="service-content-section w-full lg:pt-4">
                        <?php echo $content ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="w-full lg:w-1/2 image-section pt-3 lg:pt-0 ml-auto relative z-10">
            <img src="<?php echo $image ?>" alt="">
        </div>
    </div>
</section>
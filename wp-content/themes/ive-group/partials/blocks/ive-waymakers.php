<?php
/**
 * IVE Waymakers
 */
$image = get_field('image');
$direction = get_field('direction');
if($direction) {
    $position = 'left-0';
} else {
    $position = 'right-0';
}
?>
<section id="ive-waymakers" class="ive-waymakers relative hidden lg:block">
    <?php if($image): ?>
        <div class="decorative-holder">
            <img class="absolute <?php echo $position; ?>" src="<?php echo $image ?>" alt="">
        </div>
    <?php endif; ?>
    <div class="container hidden lg:block">
        <div class="decorative-border"></div>
    </div>
</section>

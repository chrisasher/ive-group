
<form role="search" method="get" class="search-form flex flex-wrap flex-col" action="<?php echo home_url( '/' ); ?>">
    <div>
        <input type="search" class="search-field"
            placeholder="<?php echo esc_attr_x( 'Search for something', 'placeholder' ) ?>"
            value="<?php echo get_search_query() ?>" size="15" name="wp-search-overlay"
            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
    </div>
    <!-- <input type="submit" class="search-submit ml-auto"
        value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>" /> -->
</form>
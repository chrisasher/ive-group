<?php
/**
 * Template part for displaying posts
 */
?>
<?php $image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg'; ?>
<div class="w-full lg:w-1/2 news-block-post post-<?php echo $i ?> pb-4 mt-2">
    <div class="inner flex flex-row lg:flex-row-reverse pt-3">
        <div class="w-1/2 relative post-link-holder">
            <span class="display-date"><?php echo get_the_date('d.m.Y'); ?></span>
            <div class="post-links">
                <a class="news-block-title-link" href="<?php echo get_the_permalink(); ?>"><p class="news-block-title mb-3 lg:pt-2 lg:w-5/6"><?php echo get_the_title(); ?></p></a>
                <a class="blog-arrow-link arrow-effect" href="<?php echo get_the_permalink(); ?>"><svg class="hov-effect blog-arrow" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg">
    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.14539 0L0 0L0 3.85511L10.363 8.70487L0 13.5983L0 17.4545H3.14297L16 10.9152V6.5313L3.14539 0Z" fill="#F2F2F2"/>
    </svg></a>
            </div>
        </div>
        <div class="w-1/2 mr-3 post-image-holder zoom-bg">
            <a href="<?php echo get_the_permalink(); ?>">
                <div class="cover-image w-bg-img"  style="background-image: url('<?php echo $image ?>');">
                    <!-- <img src="<?php echo $image ?>" class="mobile"> -->
                </div>
            </a>
        </div>
    </div>
</div>

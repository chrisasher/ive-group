<?php

$arrow_image = '<path fill-rule="evenodd" clip-rule="evenodd" d="M3.14539 0L0 0L0 3.85511L10.363 8.70487L0 13.5983L0 17.4545H3.14297L16 10.9152V6.5313L3.14539 0Z" fill="#031D51"/>';


$menu = wp_get_nav_menu_items('Mobile Menu');

$children = array();
foreach($menu as $key => $item){
    if ($item->menu_item_parent) {
        array_push($children, $item);
        unset($menu[$key]);
    }
}

echo '<div class="mobile-main-menu level-1">';
foreach($menu as $item){

    // get the first level items if they exist under the current parent
    $sub_menu_items = mitems_to_array($children, $item->ID);

    echo '<div class="m-menu-item '.(($item->classes[0])? $item->classes[0]: '').((count($sub_menu_items))? ' has-submenu': '').'">';
    echo '<a class="m-menu-link" href="'.$item->url.'">'.$item->title.'</a>';

    if(count($sub_menu_items)){
        echo '<a class="next-level" href="#"><svg class="arrow '.(($item->classes[0])? $item->classes[0]: '').'" width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">'.$arrow_image.'</svg></a>';
    }


    if(count($sub_menu_items)){
        echo '<div class="m-sub-menu '.(($item->classes[0])? $item->classes[0]: '').'">';
        echo '<a class="'.(($item->classes[0])? $item->classes[0]: '').' m-back-button"><svg class="arrow '.(($item->classes[0])? $item->classes[0]: '').'" width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">'.$arrow_image.'</svg> <span>Main Menu</span></a>';
        foreach($sub_menu_items as $sub_menu_item){

            // get the third menu items
            $third_menu_items = mitems_to_array($children, $sub_menu_item->ID);

            echo '<div class="m-menu-item '.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').((count($third_menu_items))? ' has-submenu': '').'">';
            echo '<a class="m-menu-link" href="'.$sub_menu_item->url.'">'.$sub_menu_item->title.'</a>';

            if(count($third_menu_items)){
                echo '<a class="next-level" href="#"><svg class="arrow '.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').'" width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">'.$arrow_image.'</svg></a>';
            }


            if(count($third_menu_items)){
                echo '<div class="m-sub-menu '.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').'">';
                //echo '<a class="'.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').' m-back-button"> < '.$item->title.'</a>';
                echo '<a class="'.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').' m-back-button"><svg class="arrow '.(($sub_menu_item->classes[0])? $sub_menu_item->classes[0]: '').'" width="16" height="18" viewBox="0 0 16 18" fill="none" xmlns="http://www.w3.org/2000/svg">'.$arrow_image.'</svg> <span>'.$item->title.'</span></a>';

                foreach($third_menu_items as $third_menu_item){
                    echo '<div class="m-menu-item '.(($third_menu_item->classes[0])? $third_menu_item->classes[0]: '').'">';
                    echo '<a class="m-menu-link" href="'.$third_menu_item->url.'">'.$third_menu_item->title.'</a>';
                    echo '</div>';
                }
                echo '</div>';
            }
            echo '</div>';
        }
        echo '</div>';
    }

    echo '</div>';

}
echo '</div>';


function mitems_to_array( $items, $parent = 0 )
{
    $bundle = [];
    foreach ( $items as $item ) {
        if ( $item->menu_item_parent == $parent ) {
            $bundle[] = $item;
        }
    }
    return $bundle;
}

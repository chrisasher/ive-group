<?php
get_header(); 

$headerTitle = get_theme_mod('blog_heading');
$description = get_theme_mod('description');
$image = get_theme_mod('header_image');
$blogHeading = get_theme_mod('latest_heading');
$gridHeading = get_theme_mod('news_grid_heading');
$contactHeading = get_theme_mod('contact_heading');
$contactImage = get_theme_mod('contact_image');
$contactShortcode = get_theme_mod('contact_shortcode');

?>

<main>
 <section id="blog-page" class="blog-page">
        
        <div class="blog-header pt-9 pb-7">
            <div class="container relative">
                <div class="w-1/2 text-white">
                    <?php if($headerTitle): ?>
                        <h1><?php echo $headerTitle ?></h1>
                    <?php endif; ?>
                    <?php if($description): ?>
                        <p><?php echo $description ?></p>
                    <?php endif; ?>
                </div>
                <?php if($image): ?>
                    <img class="absolute news-spiral" src="<?php echo $image ?>" alt="">
                <?php endif; ?>
            </div>
        </div>

        <!-- Showing the latest post -->
        <div class="latest-post container py-6 justify-center lg:mb-3">
            <!-- <h3 class="desktop"><?//php echo $blogHeading ?></h3> -->
            <?php 
            // the query
            $the_query = new WP_Query( array(
                'post_type' => 'post', 
                'posts_per_page' => 1,
                'post_status' => 'publish'
            )); 
            ?>
            <div class="latest-blog-holder flex flex-row lg:flex-row-reverse">
                <?php $featured_img_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );?>
                    <div class="w-1/2 p-6 pt-7 featured-content">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <span><?php echo get_the_date('d.m.Y'); ?></span>
                            <p class="pt-2 w-full lg:max-w-md"><?php echo get_the_title(); ?></p>
                            <div class="learn-more lg:pt-1">
                                <a href="<?php echo get_the_permalink(); ?>" class="read-more-link">Read More</a>
                            </div>
                        </a>
                    </div>
                    <div class="w-1/2 bigger-zoom">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <div class="w-bg-img bigger-cover" style="background-image: url('<?php echo $featured_img_url[0] ?>')">
                                <img src="<?php echo $featured_img_url[0] ?>" class="mobile">
                            </div>
                        </a>
                    </div>
            </div>
        </div>

        <div class="container news-grid">
            <h3><?php echo $gridHeading ?></h3>
            <div class="flex flex-col md:flex-row flex-wrap">
                <?php
                if ( have_posts() ) :
                    while ( have_posts() ) : the_post(); ?>
                    <?php $i++ ?>
                    <?php $image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg'; ?>
                    <div class="w-full lg:w-1/2 news-block-post post-<?php echo $i ?> pb-4 mt-2">
                        <div class="inner flex flex-row-reverse pt-3">
                            <div class="w-1/2 relative post-link-holder">
                                <span class="display-date"><?php echo get_the_date('d.m.Y'); ?></span>
                                <div class="post-links">
                                    <a class="news-block-title-link" href="<?php echo get_the_permalink(); ?>"><p class="news-block-title mb-3 lg:pt-2 lg:w-5/6"><?php echo get_the_title(); ?></p></a>
                                    <a class="blog-arrow-link arrow-effect" href="<?php echo get_the_permalink(); ?>"><svg class="hov-effect blog-arrow" width="16" height="18" viewBox="0 0 16 18" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.14539 0L0 0L0 3.85511L10.363 8.70487L0 13.5983L0 17.4545H3.14297L16 10.9152V6.5313L3.14539 0Z" fill="#F2F2F2"/>
                        </svg></a>
                                </div>
                            </div>
                            <div class="w-1/2 mr-3 post-image-holder zoom-bg">
                                <a href="<?php echo get_the_permalink(); ?>">
                                    <div class="cover-image w-bg-img"  style="background-image: url('<?php echo $image ?>');">
                                        <!-- <img src="<?php echo $image ?>" class="block lg:hidden"> -->
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?php endwhile;
                endif; ?>
            </div>
        </div>
        
        <!-- From https://rudrastyh.com/wordpress/load-more-posts-ajax.html -->

        <div class="container justify-start news-grid pb-7 flex flex-col md:flex-row flex-wrap fade-group">
            <div>
                <?php echo $max_num_pages ?>
            </div>
            
            <!-- don't display the button if there are not enough posts -->
            <?php if (  $wp_query->max_num_pages > 1 ): ?>
                <div class="misha_loadmore cursor-pointer pb-5 pt-4 mx-auto">More News</div>
            <?php endif; ?>
        </div>

    </section>
    <section id="contact-block" class="contact-block flex items-stretch justify-center overflow-hidden" style="background-color: #031D51;">
        <div class="container width-control flex flex-col lg:flex-row">
            <div class="w-full lg:w-1/2 image-section" style="background-image: url('<?php echo $contactImage ?>');
            background-size: cover; 
            background-position: center top;
            background-repeat: no-repeat;">
                <div class="placeholder py-10"></div>
            </div>
            <div class="w-full lg:w-1/2 form-section form-width pl-7 py-6" style="background-color:">
                <div class="w-100">
                    <h4><?php echo $contactHeading ?></h4>
                    <?php echo do_shortcode($contactShortcode) ?>
                </div>
            </div>
        </div>
    </section>
</main>
<?php get_footer(); ?>

<?php
//* Template Name: Campaign
get_header(); ?>

<?php
if (have_posts()) :
	while (have_posts()) : the_post(); ?>

		<section id="header-content-block" class="header-content-block darkblue-block" style="background-color:#031D51">
			<div class="container text-white flex flex-col lg:flex-row items-center justify-center py-5 pb-0 lg:pt-9 lg:pb-8">
				<div class="w-full lg:w-2/5">
					<div class="w-full">
						<h1 class="sitewide-header mb-4 w-full lg:w-4/5 underline-color-turquoise"><?php the_title();?></h1>
						<div class="service-content-section w-full lg:pt-4">
							<div class="MuiGrid-root-52 d-flex MuiGrid-item-54 MuiGrid-grid-xs-12-98 MuiGrid-grid-md-6-120">
								<div class="d-flex justify-content-center flex-column captionWrapper">
									<?php if (get_field('banner_content')) {
										the_field('banner_content');
									}?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="w-full lg:w-1/2 image-section pt-3 lg:pt-0 ml-auto relative z-10">
					<!-- image field -->
					<img src="http://localhost/dev/ive-group/wp-content/uploads/2021/11/banner-discount-campaign.58876b73.jpg" alt="">
				</div>
			</div>
		</section>

					<?php the_content(); ?>




<?php
	endwhile;
endif; ?>

<?php
get_footer();
